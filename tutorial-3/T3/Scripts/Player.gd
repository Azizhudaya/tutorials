extends KinematicBody2D

export (int) var speed = 500
export (int) var GRAVITY = 2000
export (int) var jump_speed = -750

const UP = Vector2(0,-1)

var velocity = Vector2()
var double_jump = true

onready var sprite = get_node("AnimatedSprite")


func get_input():
    var direction = 0
    velocity.x = 0
    
    if Input.is_action_just_pressed('up'):
        direction = 2
        if is_on_floor() : 
            velocity.y = jump_speed
            double_jump = true
        if is_on_floor() == false and double_jump:
            velocity.y = jump_speed
            double_jump = false
    
    if Input.is_action_pressed('right'):
        velocity.x += speed
        direction = 1
        sprite.set_scale(Vector2(1,1))
    elif Input.is_action_pressed('left'):
        velocity.x -= speed
        direction = -1
        sprite.set_scale(Vector2(-1,1))
	
    if is_on_floor(): 
        if(direction == 0) :
            sprite.play("idle")
        elif(abs(direction) == 1):
            sprite.play("walk")
    else:
        sprite.play("jump")
		
func _physics_process(delta):
    velocity.y += delta * GRAVITY
    get_input()
    velocity = move_and_slide(velocity, UP)
	



