# Jawaban Tutorial 2

### Nama : Aziz Fikri Hudaya
### NPM : 1706979184

# Pertanyaan : 

1. _Scene_ `BlueShip` dan `StonePlatform` sama-sama memiliki sebuah _child node_
  bertipe `Sprite`. Apa fungsi dari _node_ bertipe `Sprite`?

     > Fungsi dari `Sprite` yang menampilkan tekstur 2D. jadi apabila ingin menampilkan gambar, maka kita dapat memakai _node_ ini sebagai perantaranya.

2. _Root node_ dari _scene_ `BlueShip` dan `StonePlatform` menggunakan tipe yang
  berbeda. `BlueShip` menggunakan tipe `RigidBody2D`, sedangkan `StonePlatform`
  menggunakan tipe `StaticBody2D`. Apa perbedaan dari masing-masing tipe _node_?

    > Node `RigidBody2D` dapat memungkinkan _object_-nya dapat mengimplentasi hukum-hukum fisika, seperti gravitasi, impuls, dan tumbukan. Cocok digunakan untuk objek yang bergerak. Sementara `StaticBody2D` hampir mirip dengan `RigidBody2D`, tetapi node ini lebih cocok digunakan untuk benda yang tidak bergerak seperti tembok.

3. Ubah nilai atribut `Mass` dan `Weight` pada tipe `RigidBody2D` secara
   bebas di _scene_ `BlueShip`, lalu coba jalankan _scene_ `Main`. Apa yang
   terjadi? 

    >Saat saya mengubah nilai atribut pada `Mass`, atribut `Weight` berubah juga sesuai dengan hukum kedua Newton. Saat saya menyetel nilai atribut `Mass` rendah maupun tinggi, tidak ada perbedaan waktu jatuh bebas yang terjadi pada _spaceship_. Karena gerak jatuh bebas tidak dipengaruhi oleh `Mass`. Tetapi apabila kita menambahkan _spaceship_ lain, dan kita menset akan ada tabrakan antar spaceship, maka spaceship yang memiliki massa yang besar lebih resisten terhadap benturan. 

4. Ubah nilai atribut `Disabled` pada tipe `CollisionShape2D` di _scene_
  `StonePlatform`, lalu coba jalankan _scene_ `Main`. Apa yang terjadi?

    >_Spaceship_ tidak menabrak `StonePlatform`, tetapi _Spaceship_ akan melewati StonePlatform dan akan terus jatuh.

5. Pada _scene_ `Main`, coba manipulasi atribut `Position`, `Rotation`, dan         `Scale` milik _node_ `BlueShip` secara bebas. Apa yang terjadi pada visualisasi
  `BlueShip` di Viewport? 

    > Saat saya memanipulasi `Position`, `Rotation`, dan `Scale` pada atribut   `Blueship`, `Blueship` akan berubah sesuai informasi yang sudah dimanipulasi. Memanipulasi `Position` akan mengubah posisi di _viewport_, `Rotation` akan membuat benda berputar, dan `scale` akan memanipulasi ukuran pada _viewport_.

6. Pada _scene_ `Main`, perhatikan nilai atribut `Position` _node_ `PlatformBlue`,
  `StonePlatform`, dan `StonePlatform2`. Mengapa nilai `Position` _node_
  `StonePlatform` dan `StonePlatform2` tidak sesuai dengan posisinya di dalam
  _scene_ (menurut Inspector) namun visualisasinya berada di posisi yang tepat?
    
    >Karena `StonePlatform` dan `StonePlatform2` adalah _child node_ dari `PlatformBlue`, sehingga perubahan posisinya mengacu pada position `PlatformBlue`.

Edit : Tugas ekstra sudah diupload
